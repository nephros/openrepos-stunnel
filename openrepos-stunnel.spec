%define	upstream_name stunnel

Name:           openrepos-stunnel
Version:        5.56
Release:        1%{?dist}
Summary:        An TLS-encrypting socket wrapper
Group:          Applications/Internet
License:        GPLv2
URL:            http://www.stunnel.org/

Source0:        https://www.stunnel.org/downloads/stunnel-%{version}.tar.gz
Source1:        %{name}.service
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires:  openssl-devel
Provides: %{upstream_name}
Conflicts: %{upstream_name}

%description
Stunnel is a socket wrapper which can provide TLS (Transport Layer Security) support to ordinary applications. For example, it can be used
in conjunction with imapd to create an TLS secure IMAP server.

%prep
%autosetup -n %{upstream_name}-%{version}

%build
%configure --enable-fips --enable-ipv6 --with-ssl=%{_prefix}\
     --sysconfdir=%{_sysconfdir} \
     --runstatedir=/run \
     CPPFLAGS="-UPIDFILE -DPIDFILE='\"%{_rundir}/%{upstream_name}/%{name}.pid\"'"
%{__make} LDADD="-pie -Wl,-z,defs,-z,relro,-z,now" %{?_smp_mflags}



%install
rm -rf $RPM_BUILD_ROOT
%{__make} install DESTDIR=$RPM_BUILD_ROOT
%{__mv} $RPM_BUILD_ROOT%{_sysconfdir}/%{upstream_name}/%{upstream_name}.conf-sample\
    $RPM_BUILD_ROOT%{_sysconfdir}/%{upstream_name}/%{upstream_name}.conf
%{__install} -d $RPM_BUILD_ROOT%{_sysconfdir}/%{upstream_name}/conf.d
%{__install} -d $RPM_BUILD_ROOT%{_localstatedir}/lib/%{upstream_name}
%{__install} -p -D -m 644 %{SOURCE1} $RPM_BUILD_ROOT/%{_libdir}/systemd/system/%{upstream_name}.service
sed -i -e "s#@@BINNAME@@#%{upstream_name}#g"                          $RPM_BUILD_ROOT/%{_libdir}/systemd/system/%{upstream_name}.service
sed -i -e "s#@@CONFPATH@@#%{_sysconfdir}/%{upstream_name}#g"          $RPM_BUILD_ROOT/%{_libdir}/systemd/system/%{upstream_name}.service
sed -i -e "s#@@CONFNAME@@#%{upstream_name}.conf#g"                         $RPM_BUILD_ROOT/%{_libdir}/systemd/system/%{upstream_name}.service

%clean
rm -rf $RPM_BUILD_ROOT


%files
%defattr(-,root,root,-)
%{_bindir}/*
%{_libdir}/%{upstream_name}
%{_sysconfdir}/%{upstream_name}
%{_libdir}/systemd/system/%{upstream_name}.service
%config(noreplace) %{_sysconfdir}/%{upstream_name}/%{upstream_name}.conf
%attr(0750,nobody,nobody) %{_localstatedir}/lib/%{upstream_name}


%pre


%post
systemctl daemon-reload || :


%postun
systemctl daemon-reload || :

%preun


%changelog
* Thu Apr 23 13:29:49 CEST 2020 Nephros <sailfish@nephros.org>
- adapted from shipped spec file for SFOS
